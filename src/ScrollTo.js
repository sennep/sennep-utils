/**
 * Scroll smoothly to a given position
 * @class
 */
class ScrollTo  {
    /**
     * Shorthand function to scroll to a given position
     * @param  {Number} to       Target scroll position
     * @param  {Number} duration Duration in ms
     */
    scrollTo(to, duration) {
        var start = Math.max(document.documentElement.scrollTop, document.body.scrollTop),
            change = to - start,
            increment = 20;
            
        var animateScroll = function(elapsedTime) {        
            elapsedTime += increment;
            var position = this.easeInOut(elapsedTime, start, change, duration);                    

            window.scrollTo(0, position); 
            if (elapsedTime < duration) {
                setTimeout(function() {
                    animateScroll(elapsedTime);
                }, increment);
            }
        }.bind(this);

        animateScroll(0);
    }

    /**
     * Easing function for the scroll
     */
    easeInOut(currentTime, start, change, duration) {
        currentTime /= duration / 2;
        if (currentTime < 1) {
            return change / 2 * currentTime * currentTime + start;
        }
        currentTime -= 1;
        return -change / 2 * (currentTime * (currentTime - 2) - 1) + start;
    }
    
    


}
let single = new ScrollTo();
export default single;