/**
*   Window
*
*   Handles window related states and events. So far we handle the following:
*
*   - Window resize
*   - Window blur & focus
*/



var _size = {w:0,h:0},         // window size
    //  size vars
    _sze_clients = [],        // size subscribers
    _sze_prefix = "W_SS_",      // size prefix
    _sze_lastID = 1,          // size subscribers are assigned an id based on the previous id;
    //  focus/blur vars
    _isBlurred = false,       // wether the window is in focus or not
    _blr_clients = [],        // focus/blur subscribers
    _blr_prefix = "W_BL_",      // focus/blur prefix
    _blr_lastID = 1,          // focus/blur subscribers are assigned an id based on the previous id;
    //  retina
    _isRetina = ((window.matchMedia && (window.matchMedia('only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx), only screen and (min-resolution: 75.6dpcm)').matches || 
        window.matchMedia('only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2/1), only screen and (min--moz-device-pixel-ratio: 2), only screen and (min-device-pixel-ratio: 2)').matches)) || 
    (window.devicePixelRatio && window.devicePixelRatio >= 2)),
    _isTouch = 'ontouchstart' in window

/**
 * Update window size & call subscribers if we have a change
 * @param  {boolean} _forceUpdate Used to force update for subscribers
 */
var _updateDimensions = ( _forceUpdate ) => {
    let newSize = fetchDimensions(),
        doUpdate = _size.w!==newSize.w || _size.h!==newSize.h || _forceUpdate;

    _size.w = newSize.w;
    _size.h = newSize.h;

    if( doUpdate )
        _updateSizeSubscribers();
}

/**
 * Get window dimensions
 * @return {Object} Size object containing width and height {w:width, h:height}
 */
var fetchDimensions = () => {
    var w = window,
        d = document,
        e = d.documentElement,
        g = d.getElementsByTagName('body')[0],
        x = w.innerWidth || e.clientWidth || g.clientWidth,
        y = w.innerHeight|| e.clientHeight|| g.clientHeight;

    return {'w':x,'h':y}
}

/**
*   Update all size subscribers and pass current size object to callback
*/
var _updateSizeSubscribers = () => {
    for( let id in _sze_clients) {
        _sze_clients[id].cb( _size );
    }
}

/**
* handle window blur
**/
var _onWindowBlur = (scrollPos) => {
    _isBlurred = true;
    _updateBlurSubscribers();
}
/**
* handle window focus
**/
var _onWindowFocus = (scrollPos) => {
    _isBlurred = false;
    _updateBlurSubscribers();
}

/**
*   Update all blur subscribers with current blur state
*/
var _updateBlurSubscribers = (scrollPos) => {
    for( let id in _blr_clients) {
        _blr_clients[id].cb( _isBlurred );
    }
}

class Window  {
    constructor() {
        //  set initial values
        _size = fetchDimensions();
        //  call resize after first render
        setTimeout(()=>{ _updateDimensions() }, 10);
        //  resize event listener
        window.addEventListener("resize", _updateDimensions.bind(this))
        //  focus/blur event listener
        window.addEventListener("blur", _onWindowBlur.bind(this))
        window.addEventListener("focus", _onWindowFocus.bind(this))
    }
    
    /**
    * Sign up for window resize notification
    * @param {function} callback    Callback function
    * @return {String}              Client id
    */
    resize( callback ) {
        if( (typeof callback)!=='function' ) return;

        var id = _sze_prefix + _sze_lastID++;
        _sze_clients[id] = {
            cb: callback
        };
        return id;
    }

    /**
    * Sign up for window blur/focus notification
    * @param {function} callback    Callback function
    * @return {String}              Client id
    */
    blur( callback ) {
        if( (typeof callback)!=='function' ) return;

        var id = _blr_prefix + _blr_lastID++;
        _blr_clients[id] = {
            cb: callback
        };
        return id;
    }

	/**
    * Removes a callback based on its token.
    * @param {String}   id    Client id
    */
  	unsubscribe(id) {
        if( _sze_clients[id] ) {
            delete _sze_clients[id];
        }

        if( _blr_clients[id] ) {
            delete _blr_clients[id];
        }
  	}

    /**
    *   Check if screen is retina
    *   @return {Boolean}   Returns if screen is retina
    */

    isRetina() {
        return _isRetina;
    }

    /**
    *   Check if screen supports touch
    *   @return {Boolean}   Returns if screen supports touch
    */

    isTouch() {
        return _isTouch;
    }

    /**
    * Get current window size
    * @return {Object} Size object containing width and height {w:width, h:height}
    */

    size() {
    	return _size
    }
}

let single = new Window();

export default single;
