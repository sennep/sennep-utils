/*
*	Preloader
*
*	Preload images to reduce load time in app
*/
export default {
	loadImage( url, callback ) {
		let img = new Image();
		if(callback) {
			img.onload=callback;
		}
		img.src = url;
	}
}