
import Utils from './Utils';

/**
 * Alerts
 * 
 * Set of proxys for native/browser alerts
 * @class
 * @static
 */
let Alerts = {}

let _isApp = Utils.isApp;

/**
 * Shorthand function to open alert box in browser or native 
 * @param  {string}   message    message
 * @param  {function} callback   callback when alert is confirmed by the user
 * @param  {String}   title      title
 * @param  {String}   buttonName button label
 * @lends  Alerts
 */
let _alert = ( message, callback=()=>{}, title="Alert", buttonName="Ok" ) => {
	if( _isApp() ) {
		navigator.notification.alert(message, callback, title, buttonName);
	} else {
		alert( message );
	}
}
Alerts.alert = _alert

/**
 * Shorthand function to open confirm pop up
 * @param  {string}   message      message
 * @param  {Function} callback     callback when user cancels or confirms
 * @param  {String}   title        title
 * @param  {Array}    buttonLabels button labels. Takes two labels, usually the first is to confirm and the second to cancel
 * @lends  Alerts
 */
let _confirm = ( message, callback=()=>{}, title="Confirm", buttonLabels=["Ok","Cancel"] ) => {
	if(_isApp()) {
		navigator.notification.confirm(message, callback, title, buttonLabels)
	} else {
		var confirmed = confirm( message );
		callback( confirmed?1:2 );
	}
}
Alerts.confirm = _confirm

export default Alerts

export {_alert as Alert};

export {_confirm as Confirm};
