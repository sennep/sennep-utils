/**
 * 
 * Set of shorthand functions to make HTTP requests
 * 
 */

export default {
	/**
	 * Make HTTP GET request
	 * @param  {String}   	url     target url
	 * @param  {Function} 	success success callback
	 * @param  {Function}   error   error callback
	 */
    get( url, success=()=>{}, error=()=>{} ) {
		var request = null;
        
        if(window.XDomainRequest) {
            request = new window.XDomainRequest(); 
        } else {
            request = new window.XMLHttpRequest();
        }
        
		request.onload = function() {
			success(JSON.parse(request.responseText), request.status);	
		}
		request.onerror = function(e, b, d) {
            // console.log("ERROR", e, b, d);
			//error(JSON.parse(request.responseText), request.status);
		}
        
        request.ontimeout = function () {
            alert("Error");
        };
        // this also needs to be set
        request.onprogress = function() {
        };
        
		request.open('GET', this.getUrl(url), true);
		request.send();
    }
}