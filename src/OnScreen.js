import Raf from "./Raf";
import ReactDOM from "react-dom";

let _in = {} // components subscribed to in()
let _out = {} // components subscribed to out()
let _always = {} // components subscribed to always()
let _isRunning = false
let _prefix = "ONS_"
let _lastID = 1 // subscribers are assigned an id based on the previous id
let _raf_id = null
let _scrollPos = 0
let _lastScrollPos = 0
let _innerHeight = window.innerHeight
let body = document.body
let _timer

/**
 * OnScreen Component
 *
 *  provides callbacks for when an element enters or leaves view on scroll
 *  @class
 */
class OnScreen {

  /*
   * Constructor
   */
  constructor() {
    this.scrollHandler = this._onScroll.bind(this)
    window.addEventListener("scroll", this.scrollHandler , false)
  }

  /**
   * Subscribe elements to in() callback (Element is in view)
   * @param  {HTMLElement}    element   DOM element that we want to watch
   * @param  {Function}       callback  Callback function that is called when an element is in view
   * @param  {boolean}        full      Indicates wether the element should be completely in view
   * @return {String}                   Return client id. Should be used to unsubscribe the element
   */
  in(element, callback, full) {
    if ((typeof callback) !== "function") return

    var id = _prefix + _lastID++
    _in[id] = {
      el: element,
      cb: callback,
      full: full
    }
    this.start()
    return id
  }

  /**
   * Subscribe elements to out() callback (Element is leaving view)
   * @param  {HTMLElement}    element   DOM element that we want to watch
   * @param  {Function}       callback  Callback function that is called when an element is leaving view
   * @return {String}                   Return client id. Should be used to unsubscribe the element
   */
  out(element, callback) {
    if ((typeof callback) !== "function") return
    var id = _prefix + _lastID++
    _out[id] = {
      el: element,
      cb: callback
    }
    this.start()
    return id
  }

  /**
   * Subscribe elements to always() callback (Element is either in view or out of view)
   * @param  {HTMLElement}    element   DOM element that we want to watch
   * @param  {Function}       callback  Callback function that is called on scroll
   * @return {String}                   Return client id. Should be used to unsubscribe the element
   */
  always(callback) {
    if ((typeof callback) !== "function") return
    var id = _prefix + _lastID++
    _always[id] = {
      cb: callback
    }
    this.start()
    return id
  }
  
  /**
   * Removes element from subscribers list
   * @param  {String} id Client id received on subscription
   */
  unsubscribe(id) {
    if (_in[id]) {
      delete _in[id]
    } else if (_out[id]) {
      delete _out[id]
    } else if (_always[id]) {
      delete _always[id]
    }

    if (_in.length === 0 && _out.length === 0 && _always.length === 0) {
      this.stop()
    }
  }

  /**
   * Start tick to update scroll position and check subscribers
   */
  start() {
    if (
      (_in.length < 1 && _out.length < 1 && _always.length < 1) || _isRunning
    ) {
      return // no elements or already running -> don"t do anything
    }
    _raf_id = Raf.subscribe(this._tick.bind(this))
    _isRunning = true
    this._tick()
  }

  /**
   * Stop update tick
   */
  stop() {
    Raf.unsubscribe(_raf_id)
    window.removeEventListener("scroll", this.scrollHandler)
    _isRunning = false
  }

  /*
   * Update subscribers on tick
   * @param  {boolean} force force tick even if scroll position has not changed
   */
  _tick(force) {
    if (_scrollPos === _lastScrollPos && force !== true) {
      return
    }

    _innerHeight = window.innerHeight
    ReactDOM.unstable_batchedUpdates(() => {
      this._updateSubscribers(_scrollPos)
    })

    _lastScrollPos = _scrollPos
  }

  /*
   * Scroll event callback
   */
  _onScroll() {
    clearTimeout(_timer)
    if (body.className.indexOf("disable-hover") === -1) {
      //  remove pointer events on scroll for optimisation (make sure class is added in CSS)
      body.className = body.className + " disable-hover"
    }

    _timer = setTimeout(() => {
      body.className = body.className.replace(" disable-hover", "")
    },250);

    _scrollPos = this._scrollPosition()
  }

  /*
   * Update all subscriber according to scroll position
   * @param  {number} scrollPos Current scroll position
   */
  _updateSubscribers(scrollPos) {
    for (let id in _in) {
      let perc = this._isOnScreen(_in[id].el, _in[id].full)
      if (perc !== false) {
        _in[id].cb(perc)
      }
    }
    for (let id in _out) {
      if (this._isOnScreen(_out[id].el, false) === false) {
        _out[id].cb()
      }
    }
    for (let id in _always) {
      _always[id].cb(scrollPos)
    }
  }

  /*
   * Get current scroll position
   */
  _scrollPosition() {
    return Math.max(document.documentElement.scrollTop, body.scrollTop)
  }

  /*
   * Check wether an element is on screen
   * @param  {HTMLElement}  element  DOM element we want to test
   * @param  {boolean}      full     Indicates wether we want the DOM element to be completely visible
   * @return {Boolean}      Returns if a DOM element is on screen
   */
  _isOnScreen(element, full) {
    if (element === undefined) {
      return false
    }

    var bounds = element.getBoundingClientRect()
    var is = bounds.top < _innerHeight && bounds.bottom > 0

    if (!is) {
      return false
    }
    if (!full) {
      return true
    }

    var x = bounds.top // + ~~(bounds.height * 0.5)
    var half = ~~(_innerHeight * 0.5)
    var diff = x //half - x

    return diff / _innerHeight
  }

}

let single = new OnScreen() // nice little Singleton
export default single
