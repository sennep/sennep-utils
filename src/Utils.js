/*
* 	Checks wether we are running on the device or in a browser
*
* 	@return Boolean
*/
let _isApp = () => {
	return document.URL.indexOf( 'http://' ) === -1 && document.URL.indexOf( 'https://' ) === -1;
}

/*
*	Parse copy for dangerouslySetHtml in React
*
*	@param {String} $string String to parse
*
*	@return {String} Parsed string
*/

let _prepareHtml = (string) => {
	return {__html: ' <span>'+string+'</span>'};
}


export default {
	isApp: _isApp,
	prepareHtml: _prepareHtml
};