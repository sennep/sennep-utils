import Alerts from './Alerts'
import Api from './Api'
import OnScreen from './OnScreen'
import Preloader from './Preloader'
import Raf from './Raf'
import ScrollTo from './ScrollTo'
import BaseUtils from './Utils'
import Window from './Window'

let Utils = {}

Utils.Alerts = Alerts
Utils.Api = Api
Utils.OnScreen = OnScreen
Utils.Preloader = Preloader
Utils.Raf = Raf
Utils.ScrollTo = ScrollTo
Utils.Utils = BaseUtils
Utils.Window = Window

module.exports = Utils